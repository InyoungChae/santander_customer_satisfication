# 1. Data Prepare
def prepare_data():
    import pandas as pd
    from util.util_functions import load_data, extract_data_info_from_dataframe

    df = load_data('santander_train.csv')
    response = pd.Index(['TARGET'])

    # ratio of y(0/1)
    df_ratio = pd.DataFrame(df.TARGET.value_counts())
    df_ratio['Percentage'] = 100 * df_ratio['TARGET'] / df.shape[0]
    print(df_ratio)

    df.set_index(['ID'], inplace=True)
    df_X = df.drop(response, axis=1)
    df_y = df[response].copy()
    df_y = df_y.values.ravel()
    df_X.replace(-999999, 2, inplace=True)  # Replace -999999 in var3 column with most common value 2

    data_info = extract_data_info_from_dataframe(df_X, response)  # X_column_names, X_column_dtype

    return df_X, df_y, data_info


# 2. Model Trainer
def modeling_pipeline(df_X, df_y, data_info):
    import numpy as np
    import pandas as pd
    from util.util_functions import save_model_dill, save_model, pr_auc_score
    from sklearn.model_selection import train_test_split
    from sklearn.feature_selection import SelectKBest
    # import matplotlib.pyplot as plt
    # from sklearn.pipeline import Pipeline
    from sklearn.pipeline import FeatureUnion
    from imblearn.over_sampling import SMOTE
    from imblearn.pipeline import Pipeline
    from util.gbdt_transformer import XGBTransformer, LightGBMTransformer
    from sklearn.linear_model import LogisticRegression
    from sklearn.metrics import make_scorer, roc_auc_score, accuracy_score, f1_score
    from skopt import BayesSearchCV
    import time

    random_state = 10

    # 1. split data
    X_train, X_test, y_train, y_test = train_test_split(df_X, df_y, test_size=0.20, random_state=random_state)
    print('X_train shape:', X_train.shape)
    print('X_test shape:', X_test.shape)

    # 2. Feature Selection & GBDT_transform & logistic_regression using imblearn.pipeline
    gbdttransformer_pipeline = FeatureUnion([
        ('XGBTransformer', XGBTransformer(random_state=random_state)),
        ('LGBMTransformer', LightGBMTransformer(random_state=random_state))
    ])

    pipeline_gbdt = Pipeline([
        ('feature_selector_by_selectkbest', SelectKBest()),
        ('oversampling', SMOTE(random_state=random_state)),
        ('feature_transformer', gbdttransformer_pipeline),
        ('lr', LogisticRegression(random_state=random_state)) # penalty='l2', C=0.05,
    ])

    parameters_grid_gbdt = {
        'feature_selector_by_selectkbest__k': (20, 100),
        'feature_transformer__XGBTransformer__max_depth': (3, 8),
        'feature_transformer__LGBMTransformer__n_estimators': (10, 40),
        'lr__C': (0.1, 0.7)
    }

    bayes_search = BayesSearchCV(pipeline_gbdt, search_spaces=parameters_grid_gbdt, n_iter=10,
                                 scoring=make_scorer(pr_auc_score), verbose=10)

    start_time = time.perf_counter()
    bayes_search.fit(X_train, y_train)
    elapsed_time = time.perf_counter() - start_time
    print('BayesSearchCV taked time : ' + str(elapsed_time) + 'secs')

    best_model = bayes_search.best_estimator_
    best_parameters = bayes_search.best_params_
    print('best_parameters: ', best_parameters)

    predicted_values = best_model.predict(X_test)
    auc = roc_auc_score(y_test, predicted_values)  # AUROC
    print("ROC_AUC: ", auc)
    pr_auc = pr_auc_score(y_test, predicted_values) # AUPRC
    print("PR_AUC: ", pr_auc)
    accuracy = accuracy_score(y_test, predicted_values)
    print("ACCURACY: ", accuracy)
    f1 = f1_score(y_test, predicted_values)
    print("f1_score: ", f1)

    df_total_X = np.r_[X_train, X_test]
    df_total_y = np.r_[y_train, y_test]

    start_time = time.perf_counter()
    best_model.fit(df_total_X, df_total_y)
    elapsed_time = time.perf_counter() - start_time
    print('Final Model fitting time : ' + str(elapsed_time) + 'secs')

    coefficient = best_model.named_steps['lr'].coef_

    model_info = {
        'model': {
            'pipeline': best_model.steps
        },
        'metric': {
            'PR_AUC': pr_auc,
            'ROC_AUC': auc,
            'ACCURACY': accuracy,
            'f1_score': f1
        }
        # 'interpretation': {
        #     'feature_importances': list(zip(selected_features_by_extratrees, feature_importances))}
        }

    print(model_info)
    save_model(best_model, 'training_2_gbdt_lr_pipeline.pkl')

    return model_info


def run_training():
    df_X, df_y, data_info = prepare_data()
    model_info = modeling_pipeline(df_X, df_y, data_info)


if __name__ == '__main__':
    run_training()