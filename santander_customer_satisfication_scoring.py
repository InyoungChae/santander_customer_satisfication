# 1. Data Prepare
def prepare_data():
    import pandas as pd
    from util.util_functions import load_data, extract_data_info_from_dataframe, duplicated_values_columns

    df_X = load_data('santander_test.csv')
    df_X.set_index(['ID'], inplace=True)
    response = pd.Index(['TARGET'])

    df_X.replace(-999999, 2)  # Replace -999999 in var3 column with most common value 2
    print('original scoring_set shape:', df_X.shape)

    data_info = extract_data_info_from_dataframe(df_X, response)  # X_column_names, X_column_dtype

    return df_X, data_info


# 2. Scorer #사용자 정의 트랜스포머의 정의가 함께 있어야 unpickle 가능
from sklearn.feature_selection import SelectPercentile
from sklearn.feature_selection import f_classif


#user-defined transformer
class WrapperSelectPercentile(SelectPercentile):
    def __init__(self, score_func=f_classif, percentile=30):
        self.score_func = score_func
        self.percentile = percentile
        super(WrapperSelectPercentile, self).__init__(score_func=self.score_func, percentile=self.percentile)

    def fit(self, x, y=None):
        super(WrapperSelectPercentile, self).fit(x, y)

    def transform(self, x, y=None):
        selected_features_idx = self.get_support()
        print('f_classif selected {}%, {} features.' .format(self.percentile, selected_features_idx.sum()))
        return super(WrapperSelectPercentile, self).transform(x)

    def fit_transform(self, x, y=None):
        return super(WrapperSelectPercentile, self).fit(x, y).transform(x)


def score(df_X, data_info):
    from util.util_functions import load_model_dill, load_model

    model = load_model('training_1_random_forest_pipeline.pkl') # or 'training_2_gbdt_lr_pipeline.pkl' or 'training_3_lightgbm_pipeline.pkl'
    print(model.steps)

    predicted_score = model.predict_proba(df_X)[:,1]
    print('predicted_score:', predicted_score)
    print('predicted_score array shape: ', predicted_score.shape)

    return predicted_score


def run_scoring():
    df_X, data_info = prepare_data()
    predicted_score = score(df_X, data_info)


if __name__ == '__main__':
    run_scoring()