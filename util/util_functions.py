import os
import pandas as pd
from sklearn.externals import joblib


def load_data(file_name):
    data_path = os.path.join(os.getcwd(), 'data', file_name)
    return pd.read_csv(data_path)


def save_model(model, file_name):
    model_dir = os.path.join(os.getcwd(), 'model')
    if not os.path.isdir(model_dir):
        os.mkdir(model_dir)
    model_path = os.path.join(model_dir, file_name)
    joblib.dump(model, model_path)


def save_model_dill(model, file_name, **kwargs):
    import dill

    model_dir = os.path.join(os.getcwd(), 'model')
    if not os.path.isdir(model_dir):
        os.mkdir(model_dir)
    model_path = os.path.join(model_dir, file_name)

    with open(model_path, 'wb') as f:
        dill.dump(obj=model, file=f, **kwargs)


def load_model(file_name):
    model_dir = os.path.join(os.getcwd(), 'model')
    model_path = os.path.join(model_dir, file_name)
    return joblib.load(model_path)


def load_model_dill(file_name):
    import dill

    model_dir = os.path.join(os.getcwd(), 'model')
    model_path = os.path.join(model_dir, file_name)
    with open(model_path, 'rb') as f:
        model = dill.load(f)

    return model


def extract_data_info_from_dataframe(df, response):
    data_info = dict()
    data_info['names'] = df.dtypes.index.tolist()
    data_info['dtypes'] = df.dtypes.tolist()
    data_info['response'] = list(response)
    return data_info


def duplicated_values_columns(x):
    import numpy as np
    remove_duplicated_columns = []
    cols = list(x.columns)
    for i in range(len(cols)-1):
        v = x[cols[i]].values
        for j in range(i+1,len(cols)):
            if np.array_equal(v,x[cols[j]].values):
                remove_duplicated_columns.append(cols[j])
                
    return remove_duplicated_columns


# area under the precision recall curve
def pr_auc_score(y_true, y_score, pos_label=None, sample_weight=None):
    from sklearn.metrics import precision_recall_curve, auc
    precision, recall, thresholds = \
        precision_recall_curve(y_true, y_score, pos_label, sample_weight)
    return auc(recall, precision)





