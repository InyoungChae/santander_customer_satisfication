import numpy as np
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.ensemble import GradientBoostingClassifier
from xgboost import XGBClassifier, DMatrix
from sklearn.utils import check_array
from sklearn.utils.validation import FLOAT_DTYPES
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
import lightgbm as lgb
from abc import ABCMeta
from abc import abstractmethod
from sklearn.externals import six


# New Transformer Class GBDTTransformer
class BaseGBDTTransformer(six.with_metaclass(ABCMeta, BaseEstimator, TransformerMixin)):
    @abstractmethod
    def __init__(self, onehot=True, sparse=False,
                 **kwargs):
        # Transformer
        self.onehot = onehot
        self.sparse = sparse
        self.label_encoders = None
        self.n_values = None
        self.estimator = GradientBoostingClassifier()

    @abstractmethod
    def fit(self, X, y=None):
        if y is None:
            raise ValueError
        X = check_array(X, warn_on_dtype=True, dtype=FLOAT_DTYPES)
        self.estimator.fit(X, y)
        self._construct_encoders(X)
        return self

    def transform(self, X):
        X = check_array(X, warn_on_dtype=True, dtype=FLOAT_DTYPES)
        leaf_indices = self._decode_raw_leaves(self._pred_leaves(X))
        if self.onehot:
            return OneHotEncoder(n_values=self.n_values, sparse=self.sparse).fit_transform(leaf_indices)
        return leaf_indices

    @abstractmethod
    def _pred_leaves(self, X):
        return self.estimator.self.apply(X)[:, :, 0]

    def _construct_encoders(self, X):
        leaf_indices_raw = self._pred_leaves(X)
        # same as self.label_encoders = [LabelEncoder().fit(x) for x in leaf_indices_raw.T]
        self.label_encoders = list(map(lambda x: LabelEncoder().fit(x), leaf_indices_raw.T))
        self.n_values = list(map(lambda x: len(x.classes_), self.label_encoders))

    def _decode_raw_leaves(self, raw_leaves):
        iterator = zip(self.label_encoders, raw_leaves.T)
        return np.array(list(map(lambda x: x[0].transform(x[1]), iterator))).T


# New Transformer Class GBDTTransformer
class XGBTransformer(BaseGBDTTransformer):
    def __init__(self, onehot=True, sparse=False,
                 max_depth=3,
                 learning_rate=0.1,
                 n_estimators=100,
                 silent=True,
                 objective="binary:logistic",
                 booster='gbtree',
                 n_jobs=1,
                 nthread=None,
                 gamma=0,
                 min_child_weight=1,
                 max_delta_step=0,
                 subsample=1,
                 colsample_bytree=1,
                 colsample_bylevel=1,
                 reg_alpha=0,
                 reg_lambda=1,
                 scale_pos_weight=1,
                 base_score=0.5,
                 random_state=0,
                 seed=None,
                 missing=None,
                 **kwargs):
        # Transformer
        super().__init__(onehot=onehot, sparse=sparse)
        # XGBClassifier
        self.max_depth = max_depth
        self.learning_rate = learning_rate
        self.n_estimators = n_estimators
        self.silent = silent
        self.objective = objective
        self.booster = booster
        self.nthread = nthread
        self.n_jobs = n_jobs
        self.gamma = gamma
        self.min_child_weight = min_child_weight
        self.max_delta_step = max_delta_step
        self.subsample = subsample
        self.colsample_bytree = colsample_bytree
        self.colsample_bylevel = colsample_bylevel
        self.reg_alpha = reg_alpha
        self.reg_lambda = reg_lambda
        self.scale_pos_weight = scale_pos_weight
        self.base_score = base_score
        self.random_state = random_state
        self.seed = seed
        self.missing = missing if missing is not None else np.nan
        self.set_params(**kwargs)
        self.estimator = XGBClassifier(max_depth=self.max_depth,
                                       learning_rate=self.learning_rate,
                                       n_estimators=self.n_estimators,
                                       silent=self.silent,
                                       objective=self.objective,
                                       booster=self.booster,
                                       n_jobs=self.n_jobs,
                                       nthread=self.nthread,
                                       gamma=self.gamma,
                                       min_child_weight=self.min_child_weight,
                                       max_delta_step=self.max_delta_step,
                                       subsample=self.subsample,
                                       colsample_bytree=self.colsample_bytree,
                                       colsample_bylevel=self.colsample_bylevel,
                                       reg_alpha=self.reg_alpha,
                                       reg_lambda=self.reg_lambda,
                                       scale_pos_weight=self.scale_pos_weight,
                                       base_score=self.base_score,
                                       random_state=self.random_state,
                                       seed=self.seed,
                                       missing=self.missing)

    def fit(self, X, y=None,
            sample_weight=None,
            eval_set=None,
            eval_metric=None,
            early_stopping_rounds=None,
            verbose=True,
            xgb_model=None):
        if y is None:
            raise ValueError
        X = check_array(X, warn_on_dtype=True, dtype=FLOAT_DTYPES)
        self.estimator.fit(X, y,
                           sample_weight=sample_weight,
                           eval_set=eval_set,
                           eval_metric=eval_metric,
                           early_stopping_rounds=early_stopping_rounds,
                           verbose=verbose,
                           xgb_model=xgb_model)
        self._construct_encoders(X)
        return self

    def _pred_leaves(self, X):
        return self.estimator.get_booster().predict(DMatrix(X), pred_leaf=True)


# New Transformer Class GBDTTransformer
class LightGBMTransformer(BaseGBDTTransformer):
    def __init__(self, onehot=True, sparse=False,
                 boosting_type="gbdt",
                 num_leaves=31,
                 max_depth=-1,
                 learning_rate=0.1,
                 n_estimators=100,
                 subsample_for_bin=200000,
                 objective=None,
                 class_weight=None,
                 min_split_gain=0.,
                 min_child_weight=1e-3,
                 min_child_samples=20,
                 subsample=1.,
                 subsample_freq=1,
                 colsample_bytree=1.,
                 reg_alpha=0.,
                 reg_lambda=0.,
                 random_state=None,
                 n_jobs=-1,
                 silent=True,
                 **kwargs):
        # Transformer
        super().__init__(onehot=onehot, sparse=sparse)
        self.boosting_type = boosting_type
        self.num_leaves = num_leaves
        self.max_depth = max_depth
        self.learning_rate = learning_rate
        self.n_estimators = n_estimators
        self.subsample_for_bin = subsample_for_bin
        self.objective = objective
        self.class_weight = class_weight
        self.min_split_gain = min_split_gain
        self.min_child_weight = min_child_weight
        self.min_child_samples = min_child_samples
        self.subsample = subsample
        self.subsample_freq = subsample_freq
        self.colsample_bytree = colsample_bytree
        self.reg_alpha = reg_alpha
        self.reg_lambda = reg_lambda
        self.random_state = random_state
        self.n_jobs = n_jobs
        self.silent = silent
        self.estimator = lgb.LGBMClassifier(boosting_type=self.boosting_type,
                                            num_leaves=self.num_leaves,
                                            max_depth=self.max_depth,
                                            learning_rate=self.learning_rate,
                                            n_estimators=self.n_estimators,
                                            subsample_for_bin=self.subsample_for_bin,
                                            objective=self.objective,
                                            class_weight=self.class_weight,
                                            min_split_gain=self.min_split_gain,
                                            min_child_weight=self.min_child_weight,
                                            min_child_samples=self.min_child_samples,
                                            subsample=self.subsample,
                                            subsample_freq=self.subsample_freq,
                                            colsample_bytree=self.colsample_bytree,
                                            reg_alpha=self.reg_alpha,
                                            reg_lambda=self.reg_lambda,
                                            random_state=self.random_state,
                                            n_jobs=self.n_jobs,
                                            silent=self.silent)

    def fit(self, X, y=None,
            sample_weight=None,
            init_score=None,
            eval_set=None,
            eval_names=None,
            eval_sample_weight=None,
            eval_class_weight=None,
            eval_init_score=None,
            eval_metric="logloss",
            early_stopping_rounds=None,
            verbose=True,
            feature_name='auto',
            categorical_feature='auto',
            callbacks=None):
        if y is None:
            raise ValueError
        X = check_array(X, warn_on_dtype=True, dtype=FLOAT_DTYPES)
        self.estimator.fit(X, y,
                           sample_weight=sample_weight,
                           init_score=init_score,
                           eval_set=eval_set,
                           eval_names=eval_names,
                           eval_sample_weight=eval_sample_weight,
                           eval_class_weight=eval_class_weight,
                           eval_init_score=eval_init_score,
                           eval_metric=eval_metric,
                           early_stopping_rounds=early_stopping_rounds,
                           verbose=verbose,
                           feature_name=feature_name,
                           categorical_feature=categorical_feature,
                           callbacks=callbacks)
        self._construct_encoders(X)
        return self

    def _pred_leaves(self, X):
        return self.estimator._Booster.predict(X, pred_leaf=True)