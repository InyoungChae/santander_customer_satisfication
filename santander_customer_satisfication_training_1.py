# 1. Data Prepare
def prepare_data():
    import pandas as pd
    from util.util_functions import load_data, extract_data_info_from_dataframe, duplicated_values_columns

    df = load_data('santander_train.csv')
    response = pd.Index(['TARGET'])

    # ratio of y(0/1)
    df_ratio = pd.DataFrame(df.TARGET.value_counts())
    df_ratio['Percentage'] = 100 * df_ratio['TARGET'] / df.shape[0]
    print(df_ratio)

    df.set_index(['ID'], inplace=True)
    df_X = df.drop(response, axis=1)
    df_y = df[response].copy()
    df_y = df_y.values.ravel()
    df_X.replace(-999999, 2, inplace=True)  # Replace -999999 in var3 column with most common value 2

    data_info = extract_data_info_from_dataframe(df_X, response)  # X_column_names, X_column_dtype

    return df_X, df_y, data_info


# 2. Model Pipeline
# user-defined transformer
from sklearn.feature_selection import SelectPercentile
from sklearn.feature_selection import f_classif
from sklearn.base import BaseEstimator, TransformerMixin
from imblearn.over_sampling import RandomOverSampler


class WrapperSelectPercentile(SelectPercentile):
    def __init__(self, score_func=f_classif, percentile=30):
        self.score_func = score_func
        self.percentile = percentile
        super(WrapperSelectPercentile, self).__init__(score_func=self.score_func, percentile=self.percentile)

    def fit(self, x, y=None):
        super(WrapperSelectPercentile, self).fit(x, y)

    def transform(self, x, y=None):
        selected_features_idx = self.get_support()
        print('f_classif selected {}%, {} features.' .format(self.percentile, selected_features_idx.sum()))
        return super(WrapperSelectPercentile, self).transform(x)

    def fit_transform(self, x, y=None):
        return super(WrapperSelectPercentile, self).fit(x, y).transform(x)


# sklearn.pipeline에서는 작동하지 않음. imblearn.pipeline에서 RandomOverSampler()를 사용할 것.
# 결국 아래에 정의한 샘플러는 사용하지 않음.
class OverSampler(BaseEstimator, TransformerMixin):
    def __init__(self, random_state):
        self.random_state = random_state

    def fit(self, x, y=None):
        self.sampler = RandomOverSampler(random_state=self.random_state)
        self.sampler.fit(x, y)
        print('sampler fitting completed')
        return self

    def transform(self, x, y=None):
        if y is None:
            print('y is None')
            return x, y
        else:
            print(type(x))
            print(type(y))
            print(x.shape)
            print(y.shape)
            oversampled_x, oversampled_y = self.sampler.sample(x, y) # self.sampler.fit_sample(x, y)
            print(oversampled_y)
            # oversampled_y = oversampled_y.reshape(-1,1)
        print('sampler transform completed')
        print(type(oversampled_x))
        print(type(oversampled_y))
        return oversampled_x, oversampled_y

    # def fit_transform(self, x, y=None):
    #     oversampled_x, oversampled_y = self.sampler.fit_sample(x, y)
    #     return oversampled_x, oversampled_y


# model trainer using imblearn.pipeline
def modeling_pipeline(df_X, df_y, data_info):
    import numpy as np
    import pandas as pd
    from util.util_functions import save_model_dill, save_model, pr_auc_score
    from sklearn.model_selection import train_test_split
    from sklearn.metrics import make_scorer, roc_auc_score, accuracy_score, f1_score
    from sklearn.ensemble import ExtraTreesClassifier
    from sklearn.feature_selection import SelectFromModel
    import matplotlib.pyplot as plt
    # from sklearn.pipeline import Pipeline
    from sklearn.model_selection import GridSearchCV
    from sklearn.ensemble import RandomForestClassifier
    from imblearn.over_sampling import RandomOverSampler
    from imblearn.pipeline import Pipeline
    import time

    random_state = 10

    # 1. split data
    X_train, X_test, y_train, y_test = train_test_split(df_X, df_y, test_size=0.20, random_state=random_state)
    print('X_train shape:', X_train.shape)
    print('X_test shape:', X_test.shape)

    # 2. Feature Selection & classify using imblearn.pipeline
    pipeline_1 = Pipeline([
        ('oversampling', RandomOverSampler(random_state=random_state)),
        ('feature_selector_by_f_classif', WrapperSelectPercentile()),
        ('feature_selector_by_extratrees', SelectFromModel(ExtraTreesClassifier(random_state=random_state),
                                                           prefit=False)),
        ('classification', RandomForestClassifier(random_state=random_state))
    ])

    parameters_grid = {
              'oversampling__ratio': ['minority', 'auto'],
              'feature_selector_by_f_classif__percentile': [30, 50, 70],
              # 'feature_selector_by_extratrees__threshold': ['median', 'mean'],
              # 'feature_selector_by_extratrees__estimator__max_features': ['auto', 'sqrt', 'log2'],
              'feature_selector_by_extratrees__estimator__max_depth': [5, 7],
              # 'classification__n_estimators': [30, 50, 70],
              'classification__max_depth': [6, 8, 10]
    }

    grid_search = GridSearchCV(pipeline_1, cv=3, param_grid=parameters_grid, scoring=make_scorer(pr_auc_score))

    start_time = time.perf_counter()
    grid_search.fit(X_train, y_train)
    elapsed_time = time.perf_counter() - start_time
    print('GridSearchCV taked time : ' + str(elapsed_time) + 'secs')

    best_parameters = grid_search.best_params_
    print('best_parameters: ', best_parameters)
    best_model = grid_search.best_estimator_

    predicted_values = best_model.predict_proba(X_test)[:, 1]
    auc = roc_auc_score(y_test, predicted_values)  # AUROC
    print("ROC_AUC: ", auc)
    pr_auc = pr_auc_score(y_test, predicted_values)  # AUPRC
    print("PR_AUC: ", pr_auc)

    predicted_labels= best_model.predict(X_test)
    accuracy = accuracy_score(y_test, predicted_labels)
    print("ACCURACY: ", accuracy)
    f1 = f1_score(y_test, predicted_labels)
    print("f1_score: ", f1)

    df_total_X = np.r_[X_train, X_test]
    df_total_y = np.r_[y_train, y_test]

    start_time = time.perf_counter()
    best_model.fit(df_total_X, df_total_y)
    elapsed_time = time.perf_counter() - start_time
    print('Final Model fitting time : ' + str(elapsed_time) + 'secs')

    feature_importances = best_model.named_steps['classification'].feature_importances_

    # selected features by WrapperSelectPercentile()
    selected_by_f_classif_idx = best_model.named_steps['feature_selector_by_f_classif'].get_support()
    selected_features_by_f_classif = [f for i, f in enumerate(data_info['names']) if selected_by_f_classif_idx[i]]

    # selected features by SelectFromModel(ExtraTreesClassifier)
    selected_by_extratrees_idx = best_model.named_steps['feature_selector_by_extratrees'].get_support()
    selected_features_by_extratrees = [f for i, f in enumerate(selected_features_by_f_classif) if selected_by_extratrees_idx[i]]

    model_info = {
        'model': {
            'pipeline': best_model.steps
        },
        'metric': {
            'PR_AUC': pr_auc,
            'ROC_AUC': auc,
            'ACCURACY': accuracy,
            'f1_score': f1
        },
        'interpretation': {
            'feature_importances': list(zip(selected_features_by_extratrees, feature_importances))
        }
    }

    print(model_info)
    save_model(best_model, 'training_1_random_forest_pipeline.pkl')

    # plot most important features
    feat_imp = pd.Series(feature_importances, index=selected_features_by_extratrees).sort_values(ascending=False)
    feat_imp[:40].plot(kind='bar', title='Feature Importances', figsize=(12, 8))
    plt.ylabel('Feature Importance Score')
    plt.subplots_adjust(bottom=0.3)
    plt.savefig('Feature Importance_training_1.png')
    plt.show()

    return model_info


def run_training():
    df_X, df_y, data_info = prepare_data()
    model_info = modeling_pipeline(df_X, df_y, data_info)
    print(model_info)


if __name__ == '__main__':
    run_training()